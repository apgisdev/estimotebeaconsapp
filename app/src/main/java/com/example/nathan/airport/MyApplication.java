package com.example.nathan.airport;

/**
 * Created by Nathan on 11/17/2017.
 */

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;



import com.estimote.coresdk.common.config.EstimoteSDK;
import com.estimote.coresdk.common.requirements.SystemRequirementsChecker;
import com.estimote.coresdk.observation.region.beacon.BeaconRegion;
import com.estimote.coresdk.recognition.packets.Beacon;
import com.estimote.coresdk.service.BeaconManager;

import java.util.List;
import java.util.UUID;

public class MyApplication extends Application {

    private BeaconManager beaconManager;

    @Override
    public void onCreate() {
        Log.i("BLORG", "on create");
        EstimoteSDK.initialize(getApplicationContext(), "airport-7fb", "752a9af46425866ed3484dfcd2a32b05");

        super.onCreate();

        beaconManager = new BeaconManager(getApplicationContext());
        beaconManager.setBackgroundScanPeriod(1000,0);


        Log.i("BLORG","#1 -- HELLO NATHAN I'M HERE, lets go mario time!!!");

        beaconManager.setMonitoringListener(new BeaconManager.BeaconMonitoringListener() {
            @Override
            public void onEnteredRegion(BeaconRegion region, List<Beacon> beacons) {
                beaconManager.setRegionExitExpiration(1500);
                Log.i("BLORG", "#6 --- beacon detected function");
                Log.i("BLORG", "       they are " + beacons);
                Log.i("BLORG","        region ID = " + region.getIdentifier());
                Log.i("BLORG","        number of beacons = " + beacons.size());
                showNotification(
                        "Beacon Detected",
                        "Est-011 has entered the region");

            }
            @Override
            public void onExitedRegion(BeaconRegion region) {
                Log.i("BLORG","Beacon exited region");
                //beaconManager.stopRanging(region);
                cancelNotification();

            }
        });



        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                Log.i("BLORG","#5 --- onServiceReady .... startMonitoring new Beacon Region");

                beaconManager.startMonitoring(new BeaconRegion(
                        "monitored region",
                        UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"),
                        23968, 897));
            }
        });
    }


    public void cancelNotification() {
        Log.i("BLORG", "public void cancelNotification activated");
        showNotification(
                "Beacon Out Of Range",
                "Est-011 has exited the region");



    }

    public void showNotification(String title, String message) {
        Intent notifyIntent = new Intent(this, MainActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivities(this, 0,
                new Intent[] { notifyIntent }, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(android.R.drawable.ic_dialog_info)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build();
        notification.defaults |= Notification.DEFAULT_SOUND;
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
    }




}
