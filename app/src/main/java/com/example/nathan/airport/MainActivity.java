package com.example.nathan.airport;
import com.estimote.coresdk.common.requirements.SystemRequirementsChecker;
import com.estimote.coresdk.observation.region.beacon.BeaconRegion;
import com.estimote.coresdk.recognition.packets.Beacon;
import com.estimote.coresdk.recognition.packets.EstimoteTelemetry;
import com.estimote.coresdk.service.BeaconManager;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private BeaconManager beaconManager;
    private BeaconManager beaconTemp;
    private BeaconRegion region;
    private String scanId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("BLORG", "#2 --- creating new beacon manager");
        beaconManager = new BeaconManager(this);
        beaconTemp = new BeaconManager(this);///////////////////////////////////////////////////////
        region = new BeaconRegion("ranged region",
                UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"),
                23968, 897);

    }


    @Override
    protected void onResume() {
        Log.i("BLORG", "#3 --- super onResume function");
        super.onResume();

        SystemRequirementsChecker.checkWithDefaultDialogs(this);

        beaconManager.connect(new BeaconManager.ServiceReadyCallback() {
            @Override
            public void onServiceReady() {
                Log.i("BLORG", "#4 --- Start Ranging Region MainActivity");
                beaconManager.startRanging(region);
                beaconManager.startTelemetryDiscovery();
            }
        });
    }

    @Override
    protected void onPause() {
        Log.i("BLORG", "");
        Log.i("BLORG", "Phone paused.");
        beaconManager.stopRanging(region);


        super.onPause();
    }


}
