# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Built with Android Studio 2.3.3
* This is currently a simple Estimote App for Android testing 
* the proximity sensor of 1 beacon
* IT aslo contains a button to push in the app that currently does nothing
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Minimumn SDK 18 - JellyBean 4.3 phone or tablet
*
* Dependencies
* This only works for the APSU GIS center light blue beacon EST-011
* To configure this for beacons owned by other entities the line in MyApplication.java (originally line 33)
*        EstimoteSDK.initialize(getApplicationContext(), "airport-7fb", "752a9af46425866ed3484dfcd2a32b05");
* will have to be changed to one set up on that entities Estimote Cloud account.
* The android phone or tablet used must have bluetooth
*
* Database configuration
*
* How to run tests
* Once the app is running, if the beacon is in range and right side up (sticker side facing down)
* it should send a push notification to the device. The out of range notification will also
* trigger when the beacon is approximately 60-70 feet from the device.
* To change the beacons, the line conatining (originally line 74 in MyApplication.java):
* 	UUID.fromString("B9407F30-F5F8-466E-AFF9-25556B57FE6D"), 23968, 897));
* These can be attained from the Estimote Cloud account of the beacon -
* 	UUID - B9407F30-F5F8-466E-AFF9-25556B57FE6D
*   major - 23968
*   minor - 897
* Within the account is also the option to activate a 'sleep' mode for the beacon.
* The beacon used in this program does have sleep mode activated - this means:
* when the sticker side of the beacon is facing down (right-side up configuration)
*    the beacon will transmit its bluetooth signal to the app
* when the sticker side of the beacon is facing up (up-side down congifuration)
*    the beacon will be in a sleep mode. this allows for testing proximity
*	 and sensor settings, allowing an quasi-"on/off" switch, so that one doesn't have to
*	 walk 100 feet away every time to test certain functions.
* This means when the beacon is changes from right-side up to up-side down:
*	a push notification will be sent to the device indicating the beacon is out of range
* and when the beacon changes from up-side down to right-side up:
*	a push notification will be sent to the device indicatiing the beacon is in range
* 
*
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact